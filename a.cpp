#include <iostream>

int main() {
  int a, b, c, d;
  scanf("%d %d %d %d", &a, &b, &c, &d);
  printf("%d", ((d-b) > 0)? a+(d-b)*c : a);
}