#include <iostream>

// -----

const bool DEBUG = false;

void scanArray (int* arr, int size) {
  for (int t = 0; t < size; t++) {
    scanf("%d", arr + t);
  }
}

void printArray (int* arr, int size) {
  for (int t = 0; t < size; t++) {
    if(DEBUG) printf("%d\n", arr[t]);
  }
}

// -----

int findMinOver100(int* arr, int size) {
  for (int t = 0; t < size; t++) {
    if(DEBUG) printf("%s min at[%d] = %d\n", (arr[t] >= 100)? "Found" : "Searching", t, arr[t]);
    if (arr[t] >= 100) return t;
  }
  return size;
}

int findMaxAfter(int* arr, int size, int pos) {
  int max = -1;
  int loc = size;
  if (size <= (pos - 1)) return max;

  for (int t = (pos + 1); t < size; t++) {
    if (arr[t] > max) {
      max = arr[t];
      loc = t;
    }
  }
  return loc;
}

// -----

void remove (int* arr, int& size, int pos) {
  for (int p = pos; p < (size-1); p++) {
    arr[p] = arr[p+1];
  }
  size--;
}

int summ (int* arr, int size) {
  int sum = 0;
  for (int p = 0; p < size; p++) {
    sum += arr[p];
  }
  if(DEBUG) printf("\nRem. summ = %d\n", sum);
  return sum;
}

// -----
int dequeNextPair(int* arr, int& size) {
  int buy = findMinOver100(arr, size);

  if (buy >= (size-1)) {
    if(DEBUG) printf("No more pairs\n");
    return -1*summ(arr, size);
  } // No more pairs

  int sell = findMaxAfter(arr, size, buy);

  if (sell > buy) {

    if(DEBUG) printf("Removed: %d -- %d\n", buy, sell);
    if(DEBUG) printArray(arr, size);
    if(DEBUG) printf("------\n");

    int was = arr[buy];
    remove(arr, size, buy);
    remove(arr, size, sell - 1);

    if(DEBUG) printArray(arr, size);
    if(DEBUG) printf("------\n\n\n");

    return was;

  } else return -9999999; //error
}

int main() {
  int size = 0;
  int data[100000];
  int total = 0;

  scanf("%d", &size);
  scanArray(data, size);
  
  for (;;) {
    int result = dequeNextPair(data, size);
    if (result < 0) {
      printf("%d", total + (-1*result));
      break;
    } else {
      total += result;
    }
  }
}