#include <iostream>

int pow2above (int value) {
  int two = 1;
  for (int t = 0; t < 2000000000; t++) {
     if (two >= value) return t;
     two = two*2;
  }
  return -1; //error
}

int main() {
  int x = 0;
  scanf("%d", &x);
  printf("%d", pow2above(x));
}