#include <iostream>

float Abs (float num) {
  return (num > 0)? num : -num;
}

int main() {
  float xp, yp;
  scanf("%f %f", &xp, &yp);

  float xl, yl, xr, yr, xa, ya, xb, yb;
  scanf("%f %f %f %f %f %f %f %f", &xl, &yl, &xr, &yr, &xa, &ya, &xb, &yb);

  float Cx = xp/2;
  float Cy = yp/2;

  float cx = Abs((xa + xl)/2);
  float cy = Abs((ya + yl)/2);

  float s = Abs(xl - xa) / xp;

  float zx = (cx + Cx * s)/(1 + s);
  float zy = (cy + Cy * s)/(1 + s);

  printf("%f %f\n", zx, zy);
}